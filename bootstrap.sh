#!/bin/bash

#bootstrap script for infrastructure Design CentOS servers

# Can take a single param to allow a specific branch to be installed
BRANCH=$1;

# TO BE RUN AS ROOT
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
  echo "This script must be run as root"
  exit
fi

#VM Resize check
echo 1 > /sys/block/sdb/device/rescan
pvresize /dev/sdb
lvextend --extents +100%FREE vg_linux/lv_root --resizefs

if [ "`uname -a | grep "86_64" | wc -l`" = "1" ];
then
  PUPPETPATH="https://yum.puppetlabs.com/el/6/products/x86_64/puppetlabs-release-6-7.noarch.rpm"
else
  PUPPETPATH="https://yum.puppetlabs.com/el/6/products/i386/puppetlabs-release-6-7.noarch.rpm"
fi


# UPGRADE ALL CURRENT PACKAGES
sudo rpm -ivh $PUPPETPATH
yum update -y

# INSTALL TCPLAY if using truecrypted keyfiles
#apt-get install tcplay -y

# INSTALL GIT
yum install git -y
yum install curl -y
#yum groupinstall "Development Tools" -y

curl -sSL https://get.rvm.io | sudo bash -s stable
/usr/local/rvm/bin/rvm get stable

# INSTALL RUBY
/usr/local/rvm/bin/rvm fix-permissions system
/usr/local/rvm/bin/rvm install 2.0.0
/usr/local/rvm/bin/rvm use 2.0.0
/usr/local/rvm/bin/rvm alias create default 2.0.0
/usr/local/rvm/bin/rvm fix-permissions system
source ~/.bashrc


# INSTALL PUPPET
cd /tmp
yum install puppet -y

gem install r10k

# Clone puppet repository"
rm -rf puppet
git clone https://gitlab.msu.edu/itservicesapps/base-server.git puppet
cd puppet

if [ -n $BRANCH ]; then
  git fetch
  git checkout $BRANCH
fi

# INSTALL MODULES
r10k puppetfile install

# RUN MANIFEST
puppet apply manifests/site.pp --modulepath=modules/
#shutdown -r now
